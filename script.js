//player''s choice
var userChoice = prompt("You have one choice and you're life depends on it: Do you choose rock, paper or scissors?");
if (!userChoice) {
  document.write(
    "<p>You are most defenitly going to get eaten by zombies.</p>"
  );
} else {
  document.write("<p>You:" + " " + userChoice + "</p>");
}
//computer choice
var computerChoice = Math.random();
if (computerChoice < 0.46) {
  computerChoice = "rock";
} else if (computerChoice <= 0.55) {
  computerChoice = "paper";
} else {
  computerChoice = "scissors";
}

document.write("<p>You're sworn enemy:" + " " + computerChoice + "</p>");

var compare = function(choice1, choice2) {
  if (choice1 === choice2) {
    return "It's a tie!";
  }
  if (choice1 === "rock") {
    if (choice2 === "scissors") {
      //rock wins
      return "You get to live!";
    } else {
      //paper wins
      return "You die! Try again.";
    }
  }
  if (choice1 === "paper") {
    if (choice2 === "rock") {
      //paper wins
      return "You get to live!";
    } else {
      //scissors wins
      return "You die! Try again.";
    }
  }
  if (choice1 === "scissors") {
    if (choice2 === "rock") {
      //rock wins
      return "You die! Try again.";
    } else {
      //scissors wins
      return "You get to live!";
    }
  }
};
var results = compare(userChoice, computerChoice);
document.write("<br>" + results);
